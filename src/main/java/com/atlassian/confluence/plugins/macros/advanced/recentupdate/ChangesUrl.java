package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.PaginationParameters;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.RequestCacheThreadLocal;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;
import org.apache.commons.lang.StringUtils;

/**
 * Represents a URL to an action that produces a list of changes. Encapsulates the generation of the url from query parameters, theme and pagination parameters.
 */
public class ChangesUrl
{
    public static final Theme DEFAULT_THEME = Theme.concise;

    private final QueryParameters params;
    private final PaginationParameters paginationParameters;
    private final Theme theme;

    public ChangesUrl(QueryParameters params, PaginationParameters paginationParameters, Theme theme)
    {
        this.params = params;
        this.paginationParameters = paginationParameters;
        this.theme = theme;
    }

    /**
     * @return a relative url to changes
     */
    public String get()
    {
        final String contextPath = RequestCacheThreadLocal.getContextPath();
        StringBuilder url = new StringBuilder(contextPath == null ? "" : contextPath);
        url.append("/plugins/recently-updated/changes.action");

        url.append("?theme=").append(theme == null ? DEFAULT_THEME.name() : theme.name());

        if (paginationParameters != null)
        {
            url.append("&pageSize=").append(paginationParameters.getPageSize());
            url.append("&").append(paginationParameters.getOffSetName()).append("=").append(paginationParameters.getOffset());
        }

        if (params != null)
        {
            if (StringUtils.isNotBlank(params.getAuthors()))
                url.append("&authors=").append(GeneralUtil.urlEncode(params.getAuthors()));

            if (StringUtils.isNotBlank(params.getLabels()))
                url.append("&labels=").append(GeneralUtil.urlEncode(params.getLabels()));

            if (StringUtils.isNotBlank(params.getSpaceKeys()))
                url.append("&spaceKeys=").append(GeneralUtil.urlEncode(params.getSpaceKeys()));

            if (StringUtils.isNotBlank(params.getContentTypes()))
                url.append("&contentType=").append(params.getContentTypes());
        }

        return url.toString();
    }

    @Override
    public String toString()
    {
        return get();
    }
}
