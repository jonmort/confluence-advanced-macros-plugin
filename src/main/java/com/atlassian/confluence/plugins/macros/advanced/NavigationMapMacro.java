package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.StringHyphenBean;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.util.ContentEntityObjectTitleComparator;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

public class NavigationMapMacro extends BaseMacro
{
    private LabelManager labelManager;

    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public String execute(Map parameters, String string, RenderContext renderContext) throws MacroException
    {
        String labelName = (String) parameters.get("0");
        if (labelName == null)
        {
            throw new MacroException(ConfluenceActionSupport.getTextStatic("navmap.error.must-specify-label-name"));
        }
        String title = (String) parameters.get("title");
        Integer wrapAfter = getIntegerFromParams(parameters, "wrapAfter", 5);
        Integer cellWidth = getIntegerFromParams(parameters, "cellWidth", 90);
        Integer cellHeight = getIntegerFromParams(parameters, "cellHeight", 60);

        // estimate where to start hyphenation based on cell width
        int hyphenateAfter = StringHyphenBean.DEFAULT + 7 * (cellWidth - 90);

        Map<String, Object> contextMap = getMacroVelocityContext();
        Label label = labelManager.getLabel(labelName);
        List<ContentEntityObject> content = new ArrayList<ContentEntityObject>();
        if (label != null)
        {
            content.addAll(labelManager.getContent(label));
        }
        CollectionUtils.filter(content, new Predicate() {
            public boolean evaluate(Object object)
            {
                return object instanceof Page && !((Page) object).isDeleted();
            }
        });

        Collections.sort(content, ContentEntityObjectTitleComparator.getInstance());
        contextMap.put("pages", content);
        contextMap.put("hyphenBean", new StringHyphenBean(hyphenateAfter));
        contextMap.put("title", title);
        contextMap.put("wrapAfter", wrapAfter);
        contextMap.put("cellWidth", cellWidth);
        contextMap.put("cellHeight", cellHeight);

        return renderNavMap(parameters, contextMap);

    }

    ///CLOVER:OFF
    protected String renderNavMap(Map parameters, Map<String, Object> contextMap)
    {
        return VelocityUtils.getRenderedTemplate(getTemplate((String) parameters.get("theme")), contextMap);
    }

    protected Map<String, Object> getMacroVelocityContext()
    {
        return MacroUtils.defaultVelocityContext();
    }
    ///CLOVER:ON

    public Integer getIntegerFromParams(Map parameters, String key, int defaultValue)
    {
        try
        {
            Integer.valueOf((String) parameters.get(key));
        }
        catch (NumberFormatException e)
        {
            return defaultValue;
        }
        return Integer.valueOf((String) parameters.get(key));
    }

    private String getTemplate(String theme)
    {
        if (theme == null)
        {
            theme = "default";
        }

        return "/com/atlassian/confluence/plugins/macros/advanced/navmap-" + theme + ".vm";
    }

    public void setLabelManager(LabelManager labelManager)
    {
        this.labelManager = labelManager;
    }
}
