package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.service.NotAuthorizedException;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.renderer.RenderContext;

/**
 * Handles retrieval of a {@link ContentEntityObject} given a set of (potentially encoded/formatted) strings that
 * reference its location in Confluence.
 * @since 4.0
 */
public interface PageProvider
{
    /**
     * Maps a set of (potentially encoded/formatted) strings to a specific {@link ContentEntityObject}.
     *
     * @param location An encoded string that includes the page's title, optionally prefixed with the space name
     *                 the page is found in followed by a colon.
     *                 <br/>
     *                 For example: <code>TECH:Release Plan</code> is an encoded string that references
     *                 the "Release Plan" page in the TECH space.
     * @param context The render context of the requesting entity.
     * @return The {@link ContentEntityObject} for the correct page from the correct space.
     *         Returns null if the page cannot be found or there are permission restrictions.
     * @throws NotAuthorizedException if the user has insufficient privileges to view the space and/or page referenced.
     * @throws IllegalArgumentException if the location does not resolve to a Page or a Blog Post (see {@link AbstractPage}).
     */
    ContentEntityObject resolve(String location, RenderContext context) throws NotAuthorizedException;
}
