package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.persistence.ContentEntityObjectDao;
import com.atlassian.confluence.core.service.NotAuthorizedException;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.util.RequestCacheThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class PageIncludeServlet extends javax.servlet.http.HttpServlet
{
    private PageProvider pageProvider;
    private ContentEntityObjectDao contentEntityObjectDao;
    private I18NBeanFactory i18NBeanFactory;

    public PageIncludeServlet()
    {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        I18NBean i18NBean = i18NBeanFactory.getI18NBean();

        String location = req.getParameter("location");
        if (StringUtils.isBlank(location))
        {
            resp.sendError(400, i18NBean.getText("confluence.macros.advanced.include.error.no.location"));
            return;
        }

        long contentId;
        try
        {
            contentId = Long.parseLong(req.getParameter("contentId"));
        }
        catch (IllegalArgumentException e)
        {
            resp.sendError(400, i18NBean.getText("confluence.macros.advanced.include.error.invalid.content-id"));
            return;
        }

        ContentEntityObject ceo = contentEntityObjectDao.getById(contentId);
        if (ceo == null)
        {
            resp.sendError(400, i18NBean.getText("confluence.macros.advanced.include.error.no.content-entity"));
            return;
        }

        try
        {
            PageContext context = new PageContext(ceo);
            ContentEntityObject page = pageProvider.resolve(location, context);
            if (page == null)
            {
                resp.sendError(404);
                return;
            }
            final String baseUrl = RequestCacheThreadLocal.getContextPath();
            resp.sendRedirect(baseUrl + page.getUrlPath());
        }
        catch (IllegalArgumentException e)
        {
            resp.sendError(404, e.getMessage());
        }
        catch (NotAuthorizedException e)
        {
            resp.sendError(404, i18NBean.getText("confluence.macros.advanced.include.error.content.not.found"));
        }
    }

    public void setPageProvider(PageProvider pageProvider)
    {
        this.pageProvider = pageProvider;
    }

    public void setContentEntityObjectDao(ContentEntityObjectDao contentEntityObjectDao)
    {
        this.contentEntityObjectDao = contentEntityObjectDao;
    }

    public void setUserI18NBeanFactory(I18NBeanFactory i18NBeanFactory)
    {
        this.i18NBeanFactory = i18NBeanFactory;
    }
}
