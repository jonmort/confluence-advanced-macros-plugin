package com.atlassian.confluence.plugins.macros.advanced;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputDeviceType;
import com.atlassian.confluence.content.render.xhtml.LinkRenderer;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.macros.advanced.xhtml.ChildrenMacro;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.ExcerptHelper;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;

import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

public class ChildrenMacroTestCase extends TestCase
{
    private ChildrenMacro pageChildrenMacro;
    
    private Map<String, String> parameters = new HashMap<String, String>();
    
    private String spaceKey = "tst";
    private Space space;
    private Page parentPage, childPageOne, childPageLevelOne, childPageTwo;
    
    @Mock private MacroExecutionContext ctx;
    @Mock private ContainerContext containerContext;
    @Mock private PageContext pageContext;
    @Mock private PageManager pageManager;
    @Mock private PermissionManager permissionManager;
    @Mock private ContentPermissionManager contentPermissionManager;
    @Mock private LinkRenderer viewLinkRenderer;
    @Mock private ExcerptHelper excerptHelper;
    @Mock private SpaceContentEntityObject spaceContentEntityObject;
    @Mock private SpaceManager spaceManager;
    @Mock private ConfluenceActionSupport confluenceActionSupport;
    @Mock private WebResourceManager webResourceManager;
    
    @Override
    protected void setUp() throws Exception 
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        ContainerManager.getInstance().setContainerContext(containerContext);
        
        space = new Space();
        space.setName("Test");
        space.setKey("tst");
      
        parentPage = new Page();
        parentPage.setTitle("Parent Page");
        parentPage.setSpace(space);
      
        childPageOne = new Page();
        childPageOne.setTitle("Apple");
        childPageOne.setParentPage(parentPage);
        childPageOne.setSpace(space);
        parentPage.addChild(childPageOne);
        
        childPageLevelOne = new Page();
        childPageLevelOne.setTitle("ApplePie");
        childPageLevelOne.setParentPage(childPageOne);
        childPageLevelOne.setSpace(space);
        childPageOne.addChild(childPageLevelOne);
      
        childPageTwo = new Page();
        childPageTwo.setTitle("Banana");
        childPageTwo.setParentPage(parentPage);
        childPageTwo.setSpace(space);
        parentPage.addChild(childPageTwo);

        when(pageContext.getOutputDeviceType()).thenReturn(ConversionContextOutputDeviceType.DESKTOP);
        when(ctx.getPageContext()).thenReturn(pageContext);
        when(ctx.getParams()).thenReturn(parameters);
        
        when(pageContext.getEntity()).thenReturn(parentPage);
        
        when(permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, parentPage)).thenReturn(true);
        when(permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, childPageOne)).thenReturn(true);
        when(permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, childPageTwo)).thenReturn(true);
        
        when(contentPermissionManager.getPermittedChildren(eq(parentPage), eq(AuthenticatedUserThreadLocal.getUser()))).
        thenReturn(Arrays.asList(new Page[] {childPageOne, childPageTwo}));
        when(contentPermissionManager.getPermittedChildren(eq(childPageOne), eq(AuthenticatedUserThreadLocal.getUser()))).
        thenReturn(Arrays.asList(new Page[] {childPageLevelOne}));
        when(contentPermissionManager.getPermittedChildren(eq(childPageTwo), eq(AuthenticatedUserThreadLocal.getUser()))).
        thenReturn(Collections.<Page>emptyList());
        
        when(viewLinkRenderer.render(eq(childPageOne), (ConversionContext)anyObject())).thenReturn("<a href=\"/display/" + spaceKey + "/" + childPageOne.getTitle() + "\"></a>");

        when(viewLinkRenderer.render(eq(childPageTwo), (ConversionContext)anyObject())).thenReturn("<a href=\"/display/" + spaceKey + "/" + childPageTwo.getTitle() + "\"></a>");
        when(viewLinkRenderer.render(eq(childPageLevelOne), (ConversionContext)anyObject())).thenReturn("<a href=\"/display/" + spaceKey + "/" + childPageTwo.getTitle() + "\"></a>");
    }

    @Override
    protected void tearDown() throws Exception 
    {
        ContainerManager.getInstance().setContainerContext(null);
        
        super.tearDown();
    }
    
    public void testWithoutAnyAttribute() throws Exception
    {
        pageChildrenMacro = new TestChildrenMacro();
        pageChildrenMacro.setViewLinkRenderer(viewLinkRenderer);
        
        assertEquals("<ul class='childpages-macro'><li><a href=\"/display/tst/Apple\"></a></li><li><a href=\"/display/tst/Banana\"></a></li></ul>", pageChildrenMacro.execute(ctx));
    }
    
    public void testWithAllAttribute() throws Exception
    {
        pageChildrenMacro = new TestChildrenMacro();
        pageChildrenMacro.setViewLinkRenderer(viewLinkRenderer);
        
        parameters.put("all", "true");
        
        when(permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, childPageLevelOne)).thenReturn(true);
        
        when(viewLinkRenderer.render(eq(childPageLevelOne), (ConversionContext)anyObject())).thenReturn("<a href=\"/display/" + spaceKey + "/" + childPageLevelOne.getTitle() + "\"></a>");
        
        assertEquals("<ul class='childpages-macro'><li><a href=\"/display/tst/Apple\"></a><ul class='childpages-macro'><li><a href=\"/display/tst/ApplePie\"></a></li></ul></li><li><a href=\"/display/tst/Banana\"></a></li></ul>", pageChildrenMacro.execute(ctx));
    }
    
    public void testWithDepthAndStyleAttribute() throws Exception
    {
        pageChildrenMacro = new TestChildrenMacro();
        pageChildrenMacro.setViewLinkRenderer(viewLinkRenderer);
        
        parameters.put("depth", "1");
        parameters.put("style", "h2");
        
        assertEquals("<h2><a href=\"/display/tst/Apple\"></a></h2>\n<h2><a href=\"/display/tst/Banana\"></a></h2>\n", pageChildrenMacro.execute(ctx));
    }
    
    public void testWithExcerptAttribute() throws Exception
    {
        pageChildrenMacro = new TestChildrenMacro();
        pageChildrenMacro.setViewLinkRenderer(viewLinkRenderer);
        
        parameters.put("excerpt", "true");
        
        when(excerptHelper.getExcerptSummary(childPageOne)).thenReturn(childPageOne.getTitle());
        when(excerptHelper.getExcerptSummary(childPageTwo)).thenReturn(childPageTwo.getTitle());
        
        assertEquals("<ul class='childpages-macro'><li><a href=\"/display/tst/Apple\"></a> &mdash; <span class=\"smalltext\">Apple</span></li><li><a href=\"/display/tst/Banana\"></a> &mdash; <span class=\"smalltext\">Banana</span></li></ul>", pageChildrenMacro.execute(ctx));
    }
    
    public void testWithPageAttribute() throws Exception
    {
        pageChildrenMacro = new TestChildrenMacro();
        
        parameters.put("page", "Dog");
        
        Space space = new Space();
        space.setName("Animal");
        space.setKey("ani");
        
        Page dogPage = new Page();
        dogPage.setTitle("Dog");
        dogPage.setSpace(space);
        
        Page puppyPage = new Page();
        puppyPage.setTitle("Puppy");
        puppyPage.setSpace(space);
        puppyPage.setParentPage(dogPage);
        dogPage.addChild(puppyPage);
        
        when(pageContext.getSpaceKey()).thenReturn(spaceKey);
        when(pageManager.getPage(spaceKey, dogPage.getTitle())).thenReturn(dogPage);
        when(permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, dogPage)).thenReturn(true);
        when(permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, puppyPage)).thenReturn(true);
        when(contentPermissionManager.getPermittedChildren(eq(dogPage), eq(AuthenticatedUserThreadLocal.getUser()))).
        thenReturn(Arrays.asList(new Page[] {puppyPage}));
        when(viewLinkRenderer.render(eq(puppyPage), (ConversionContext)anyObject())).thenReturn("<a href=\"/display/" + spaceKey + "/" + puppyPage.getTitle() + "\"></a>");
        
        assertEquals("<ul class='childpages-macro'><li><a href=\"/display/tst/Puppy\"></a></li></ul>", pageChildrenMacro.execute(ctx));
    }
    
    public void testWithPageAttributeWhichListAllPagesInTheSpaceWithNoParent() throws Exception
    {
        pageChildrenMacro = new TestChildrenMacro();
        
        parameters.put("page", "/");
        
        List<Page> pages = new ArrayList<Page>();
        pages.add(parentPage);
        
        when(permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.VIEW, space)).thenReturn(true);
        when(pageManager.getTopLevelPages(space)).thenReturn(pages);
        
        when(pageContext.getEntity()).thenReturn(spaceContentEntityObject).thenReturn(null);
        when(spaceContentEntityObject.getSpace()).thenReturn(space);
        
        when(viewLinkRenderer.render(eq(parentPage), (ConversionContext)anyObject())).thenReturn("<a href=\"/display/" + spaceKey + "/" + parentPage.getTitle() + "\"></a>");
        when(permissionManager.getPermittedEntities(eq(AuthenticatedUserThreadLocal.getUser()), eq(Permission.VIEW), eq(pages))).thenReturn(pages);
        
        assertEquals("<ul class='childpages-macro'><li><a href=\"/display/" + spaceKey + "/" + parentPage.getTitle() + "\"></a></li></ul>",
                pageChildrenMacro.execute(ctx));
    }
    
    public void testWithFirstAttribute() throws Exception
    {
        pageChildrenMacro = new TestChildrenMacro();
        
        parameters.put("first", "1");
        
        assertEquals("<ul class='childpages-macro'><li><a href=\"/display/tst/Apple\"></a></li></ul>", pageChildrenMacro.execute(ctx));
    }
    
    public void testWithSortAndReverAttribute() throws Exception
    {
        pageChildrenMacro = new TestChildrenMacro();
        
        parameters.put("sort", "title");
        parameters.put("reverse", "true");
        
        assertEquals("<ul class='childpages-macro'><li><a href=\"/display/tst/Banana\"></a></li><li><a href=\"/display/tst/Apple\"></a></li></ul>", pageChildrenMacro.execute(ctx));
    }
    
    public void testIsInline()
    {
        pageChildrenMacro = new TestChildrenMacro();
        
        assertFalse(pageChildrenMacro.isInline());
    }
    
    public void testHasBody()
    {
        pageChildrenMacro = new TestChildrenMacro();
        
        assertFalse(pageChildrenMacro.hasBody());
    }
    
    public void testBodyRenderMode()
    {
        pageChildrenMacro = new TestChildrenMacro();
        
        assertEquals(RenderMode.NO_RENDER, pageChildrenMacro.getBodyRenderMode());
    }
    
    private class TestChildrenMacro extends ChildrenMacro
    {
        public TestChildrenMacro()
        {
            setPageManager(pageManager);
            setPermissionManager(permissionManager);
            setViewLinkRenderer(viewLinkRenderer);
            setExcerptHelper(excerptHelper);
            setSpaceManager(spaceManager);
            setWebResourceManager(webResourceManager);
            setContentPermissionManager(contentPermissionManager);
        }

        @Override
        protected ConfluenceActionSupport getConfluenceActionSupport()
        {
            return confluenceActionSupport;
        }
    }
}
