package com.atlassian.confluence.plugins.macros.advanced;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import junit.framework.TestCase;

import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.core.service.NotAuthorizedException;
import com.atlassian.confluence.links.linktypes.PageLink;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.ContentIncludeStack;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.util.i18n.Message;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.links.LinkResolver;
import com.atlassian.renderer.links.UnpermittedLink;
import com.atlassian.renderer.links.UnresolvedLink;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;

public class PageIncludeMacroTestCase extends TestCase
{
    private PageProvider pageProvider;
    @Mock private LinkResolver linkResolver;
    @Mock private PermissionManager permissionManager;
    @Mock private Renderer viewRenderer;
    @Mock private I18NBeanFactory i18NBeanFactory;

    private final String PAGE_TITLE = "the page title";
    private final String SPACE_KEY = "tst";
    private static final String ERROR_PREFIX = "confluence.macros.advanced.include.unable-to-render";

    private Space space;
    private Page pageToRenderOn;
    private Page includedPage;
    private RenderContext renderContext;

    private Map<String, String> macroParams;

    private PageIncludeMacro pageIncludeMacro;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        space = new Space(SPACE_KEY);
        pageToRenderOn = new Page();
        pageToRenderOn.setSpace(space);
        renderContext = pageToRenderOn.toPageContext();

        when(permissionManager.hasPermission(Matchers.<User>anyObject(), Matchers.eq(Permission.VIEW), Matchers.<PageLink>anyObject())).thenReturn(true);

        when(i18NBeanFactory.getI18NBean()).thenReturn(new TestI18NBean());

        includedPage = new Page();
        macroParams = new HashMap<String, String>();

        pageProvider = new TestPageProvider();
        pageIncludeMacro = new TestPageIncludeMacro();
    }

    public void testErrorShownIfUsedOnNonPages() throws MacroException
    {
        RenderContext renderContext = mock(RenderContext.class);

        assertEquals(
                renderErrorMessage("confluence.macros.advanced.include.error.can-only-be-used-in-confluence"),
                pageIncludeMacro.execute(macroParams, null, renderContext)
        );
    }

    public void testErrorThrownIfPageTitleNotSpecified() throws MacroException
    {
        try
        {
            pageIncludeMacro.execute(macroParams, null, pageToRenderOn.toPageContext());
            fail();
        }
        catch (MacroException e)
        {
            assertEquals(
                    "confluence.macros.advanced.include.error.no.page-title",
                    e.getMessage());
        }
    }

    public void testDefaultParameterReadAsPageTitleWhenPageTitleParameterNotSpecified() throws MacroException, ParseException
    {
        String fakeOutput = "fakeOutput";
        String expectedLocation = PAGE_TITLE;

        includedPage.setSpace(space);

        when(linkResolver.createLink(renderContext, expectedLocation)).thenReturn(PageLink.makeTestLink(expectedLocation, includedPage.toPageContext()));
        when(viewRenderer.render(anyString(), (ConversionContext) anyObject())).thenReturn(fakeOutput);

        macroParams.put("0", PAGE_TITLE);

        assertEquals(
                fakeOutput,
                pageIncludeMacro.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testPageTitleParamPreferredOverDefaultParameterForPageTitle() throws MacroException, ParseException
    {
        String defaultPageTitle = "defaultPageTitle";
        String fakeOutput = "fakeOutput";
        String expectedLocation = PAGE_TITLE;

        includedPage.setSpace(space);

        verify(linkResolver, times(0)).createLink(renderContext, defaultPageTitle);
        when(linkResolver.createLink(renderContext, expectedLocation)).thenReturn(PageLink.makeTestLink(expectedLocation,includedPage.toPageContext()));
        when(viewRenderer.render(anyString(), (ConversionContext) anyObject())).thenReturn(fakeOutput);

        macroParams.put("0", defaultPageTitle);
        macroParams.put("pageTitle", expectedLocation);

        assertEquals(
                fakeOutput,
                pageIncludeMacro.execute(macroParams, null, renderContext));
    }

    public void testNotFoundErrorShownIfPageDoesNotExistInSpace() throws Exception
    {
        String expectedLocation = "foo page";

        includedPage.setSpace(space);

        when(linkResolver.createLink(renderContext, expectedLocation)).thenReturn(new UnresolvedLink(expectedLocation, PageLink.makeTestLink(expectedLocation, includedPage.toPageContext())));

        macroParams.put("0", expectedLocation);

        assertEquals(
                renderErrorMessage("confluence.macros.advanced.include.error.content.not.found"),
                pageIncludeMacro.execute(macroParams, null, renderContext));
    }

    public void testNotFoundErrorShownIfSpaceDoesNotExist() throws Exception
    {
        final String nonExistentSpace = "foospace";
        String expectedLocation = nonExistentSpace + ":" + PAGE_TITLE;

        includedPage.setSpace(space);

        when(linkResolver.createLink(renderContext, expectedLocation)).thenReturn(new UnresolvedLink(expectedLocation, PageLink.makeTestLink(expectedLocation, includedPage.toPageContext())));

        macroParams.put("0", expectedLocation);

        assertEquals(
                renderErrorMessage("confluence.macros.advanced.include.error.content.not.found"),
                pageIncludeMacro.execute(macroParams, null, renderContext));
    }

    public void testErrorShownIfUserDoesNotHavePermissionToViewSpace() throws MacroException, ParseException
    {
        final String expectedLocation = PAGE_TITLE;

        includedPage.setSpace(space);

        final UnpermittedLink pageLink = new UnpermittedLink(PageLink.makeTestLink(expectedLocation, includedPage.toPageContext()));
        when(linkResolver.createLink(renderContext, expectedLocation)).thenReturn(pageLink);

        // The macro should report the page wasn't found, even though it's a permissions restriction.
        macroParams.put("0", expectedLocation);
        assertEquals(
                renderErrorMessage("confluence.macros.advanced.include.error.content.not.found"),
                pageIncludeMacro.execute(macroParams, null, renderContext));

        // Make sure that it wasn't found because of a permissions restriction.
        try
        {
            pageProvider.resolve(expectedLocation, renderContext);
            fail();
        }
        catch (NotAuthorizedException e)
        {
            assertEquals("confluence.macros.advanced.include.error.user.not.authorized", e.getMessage());
        }
    }

    public void testErrorShownIfUserDoesNotHavePermissionToViewPage() throws MacroException, ParseException
    {
        includedPage.setSpace(space);

        final String expectedLocation = PAGE_TITLE;
        final PageLink pageLink = PageLink.makeTestLink(expectedLocation, includedPage.toPageContext());

        when(linkResolver.createLink(renderContext, expectedLocation)).thenReturn(pageLink);
        when(permissionManager.hasPermission(Matchers.<User>anyObject(), Matchers.eq(Permission.VIEW), Matchers.eq(pageLink.getDestinationContent()))).thenReturn(false);

        // The macro should report the page wasn't found, even though it's a permissions restriction.
        macroParams.put("0", expectedLocation);
        assertEquals(
                renderErrorMessage("confluence.macros.advanced.include.error.content.not.found"),
                pageIncludeMacro.execute(macroParams, null, renderContext));

        // Make sure that it wasn't found because of a permissions restriction.
        try
        {
            pageProvider.resolve(expectedLocation, renderContext);
            fail();
        }
        catch (NotAuthorizedException e)
        {
            assertEquals("confluence.macros.advanced.include.error.user.not.authorized", e.getMessage());
        }
    }

    public void testErrorShownIfPageToIncludeIsAlreadyIncluded() throws MacroException, ParseException
    {
        includedPage.setSpace(space);
        includedPage.setTitle(PAGE_TITLE);

        when(linkResolver.createLink(renderContext, PAGE_TITLE)).thenReturn(PageLink.makeTestLink(PAGE_TITLE, includedPage.toPageContext()));

        macroParams.put("0", PAGE_TITLE);

        try
        {
            ContentIncludeStack.push(includedPage);

            assertEquals(
                    renderErrorMessage("confluence.macros.advanced.include.error.already.included"),
                    pageIncludeMacro.execute(macroParams, null, renderContext));
        }
        finally
        {
            ContentIncludeStack.pop();
        }
    }

    public void testIncludePageFromAnotherSpace() throws MacroException, ParseException
    {
        Space anotherSpace = new Space("tst2");
        String fakeOutput = "fakeOutput";
        String expectedLocation = anotherSpace.getKey() + ":" + PAGE_TITLE;

        includedPage.setSpace(anotherSpace);

        when(linkResolver.createLink(renderContext, expectedLocation)).thenReturn(PageLink.makeTestLink(expectedLocation,includedPage.toPageContext()));
        when(viewRenderer.render(anyString(), (ConversionContext) anyObject())).thenReturn(fakeOutput);

        macroParams.put("0", expectedLocation);

        assertEquals(
                fakeOutput,
                pageIncludeMacro.execute(macroParams, null, renderContext));
    }

    public void testIncludePageFromAnotherSpaceUsingSpaceKeyAndPageTitleParams() throws MacroException, ParseException
    {
        Space anotherSpace = new Space("tst2");
        String fakeOutput = "fakeOutput";
        String expectedLocation = anotherSpace.getKey() + ":" + PAGE_TITLE;

        includedPage.setSpace(anotherSpace);

        when(linkResolver.createLink(renderContext, expectedLocation)).thenReturn(PageLink.makeTestLink(expectedLocation,includedPage.toPageContext()));
        when(viewRenderer.render(anyString(), (ConversionContext) anyObject())).thenReturn(fakeOutput);

        macroParams.put("spaceKey", anotherSpace.getKey());
        macroParams.put("pageTitle", PAGE_TITLE);

        assertEquals(
                fakeOutput,
                pageIncludeMacro.execute(macroParams, null, pageToRenderOn.toPageContext()));
    }

    public void testRenderBlock()
    {
        assertFalse(pageIncludeMacro.isInline());
    }

    public void testHasNoBody()
    {
        assertFalse(pageIncludeMacro.hasBody());
    }

    public void testDoesNotRenderBody()
    {
        assertEquals(RenderMode.NO_RENDER, pageIncludeMacro.getBodyRenderMode());
    }
    
    // ADVMACROS-226
    public void testIncludedPageRenderMode() throws ParseException, MacroException
    {
    	includedPage.setSpace(space);
    	includedPage.setTitle(PAGE_TITLE);

    	when(linkResolver.createLink(renderContext, PAGE_TITLE)).thenReturn(PageLink.makeTestLink(PAGE_TITLE, includedPage.toPageContext()));

    	macroParams.put("0", PAGE_TITLE);

    	RenderContext pageToRenderOnPageContext = pageToRenderOn.toPageContext();
    	pageToRenderOnPageContext.setOutputType("pdf");
    	
    	pageIncludeMacro.execute(macroParams, null, pageToRenderOnPageContext); 
         
    	ArgumentCaptor<DefaultConversionContext> captor = ArgumentCaptor.forClass(DefaultConversionContext.class);
    	verify(viewRenderer).render(anyString(), captor.capture());
    	DefaultConversionContext context = captor.getValue();
    	assertEquals("pdf", context.getOutputType());
    }
    
    private static String renderErrorMessage(String message)
    {
        return (message != null) ? "<div class=\"error\"><span class=\"error\">" + ERROR_PREFIX + "</span> " + message + "</div>" : "";
    }


    private class TestPageProvider extends DefaultPageProvider
    {
        private TestPageProvider()
        {
            setLinkResolver(linkResolver);
            setPermissionManager(permissionManager);
            setUserI18NBeanFactory(i18NBeanFactory);
        }
    }

    private class TestPageIncludeMacro extends PageIncludeMacro
    {
        private TestPageIncludeMacro()
        {
            setPageProvider(pageProvider);
            setViewRenderer(viewRenderer);
            setUserI18NBeanFactory(i18NBeanFactory);
        }
    }

    private class TestI18NBean implements I18NBean
    {
        public String getText(String s)
        {
            return s;
        }

        public String getText(String s, Object[] objects)
        {
            return s;
        }

        public String getText(String s, List list)
        {
            return s;
        }

        public String getText(Message message)
        {
            return message.toString();
        }

        public String getTextStrict(String s)
        {
            return s;
        }

        public boolean isI18nHighlightMode()
        {
            return false;
        }

        public boolean isShowKeyMode()
        {
            return false;
        }

        public ResourceBundle getResourceBundle()
        {
            return null;
        }

        public Map<String, String> getTranslationsForPrefix(String s)
        {
            return null;
        }

        @Override
        public String getUntransformedRawText(String s)
        {
            return StringUtils.EMPTY;
        }
    }
}
