package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import bucket.core.persistence.hibernate.HibernateHandle;
import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.Page;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.StartHandlePaginationParameters;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.pagination.StartIndexPaginationParameters;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.SearchResults;
import org.mockito.Mock;
import static org.mockito.Mockito.when;

import java.util.Collections;

public class DefaultRecentUpdatesManagerTestCase extends AbstractTestCase
{
    @Mock SearchManager searchManager;
    @Mock RecentChangesSearchBuilder searchBuilder;
    @Mock ISearch search;
    @Mock SearchResult searchResult;
    @Mock SearchResults searchResults;

    private DefaultRecentUpdatesManager recentUpdatesManager;
    private HibernateHandle handle;
    private StartIndexPaginationParameters paginationParams;
    private QueryParameters queryParams;

    public void testNextPageParametersSetWhenThereAreMorePages() throws Exception
    {
        when(searchResults.getAll()).thenReturn(Collections.nCopies(11, searchResult));
        Page<SearchResult> page = recentUpdatesManager.find(queryParams, paginationParams);

        assertTrue(page.getNextPageParameters() instanceof StartHandlePaginationParameters);
        assertEquals(handle, ((StartHandlePaginationParameters) page.getNextPageParameters()).getResultFilter().getHandle());
    }

    public void testNextPageParametersNotSetWhenThereAreNoMorePages() throws Exception
    {
        when(searchResults.getAll()).thenReturn(Collections.nCopies(5, searchResult));
        Page<SearchResult> page = recentUpdatesManager.find(queryParams, paginationParams);

        assertFalse(page.morePages());
        try
        {
            page.getNextPageParameters();
            fail();
        }
        catch (IllegalStateException e)
        {
            // expected
        }
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        recentUpdatesManager = new DefaultRecentUpdatesManager(searchManager, searchBuilder);
        handle = new HibernateHandle(Page.class.getName() + "-123");
        when(searchResult.getHandle()).thenReturn(handle);
        paginationParams = new StartIndexPaginationParameters(0, 10);
        queryParams = new QueryParameters(null, null, null, null);
        when(searchBuilder.getChangesSearch(queryParams, paginationParams)).thenReturn(search);
        when(searchManager.search(search)).thenReturn(searchResults);
    }
}
