package it.com.atlassian.confluence.plugins.macros.advanced;

import java.io.IOException;

import org.apache.xmlrpc.XmlRpcException;

public class ChildrenMacroFuncTest extends AbstractConfluencePluginWebTestCaseBase {


    public static final String TEST_SPACE2_TITLE = "Test Space 2"; //getTestProperty("testspace.title");
    public static final String TEST_SPACE2_KEY = "tst2";	//getTestProperty("testspace2.key");
    public static final String TEST_SPACE2_DESCRIPTION = "Test Space 2";	//getTestProperty("testspace2.description");
    
    public static final String TESTPAGE_TITLE = "Test Page"; //getTestProperty("testpage.title");


	/**
     * The length of time to pause between actions must be longer for some databases than others.  Specifically, we
     * know that we store dates in 1 second granularity in Oracle.
     */
    protected static final int PAUSE_MILLIS = 10;	//getTestPropertyAsInt("pause.time.millis", 10);
    protected static final int PAUSE_MYSQL_MILLIS = 1000; // MySQL datetime type has a 1 second granularity
	private static final String TEST_GEN_USERNAME1 = "user_one";
	private static final String TEST_GEN_PASSWORD1 = "password_one";

    // TODOXHTML CONFDEV-1224
    // CONFDEV-273
    public void TODOXHTML_testChildrenMacro() throws Exception
    {
        // Setup test variables
        String parentPageName = "parentPage";
        String childPageName = "childPage";
        String grandChildPageName = "grandPage";

        // Add parent page
        long parentId = createPage(TEST_SPACE_KEY, parentPageName, "{children:all=true}");
        long childId = createPage(TEST_SPACE_KEY, childPageName, "{children}", parentId);
        createPage(TEST_SPACE_KEY, grandChildPageName, "grand child page", childId);

        // View Page Test
        viewPage(TEST_SPACE_KEY, parentPageName);

        // Now check that child and grand child page appears in the parent page from use of children macro
        assertTextPresent(childPageName);
        assertTextPresent(grandChildPageName);

        // View Page Test
        viewPage(TEST_SPACE_KEY, childPageName);

        // Now check that the child page shows the grand child page
        assertTextPresent(parentPageName);
        assertTextPresent(grandChildPageName);
    }

    // TODOXHTML CONFDEV-1224
    public void TODOXHTML_testChildrenMacroWithDefinedDepth() throws Exception
    {
        // Setup test variables
        String parentPageName = "parentPage";
        String childPageName = "childPage";
        String grandChildPageName = "grandPage";

        long parentId = createPage(TEST_SPACE_KEY, parentPageName, "{children:depth=1}");
        long childId = createPage(TEST_SPACE_KEY, childPageName, "child page", parentId);
        createPage(TEST_SPACE_KEY, grandChildPageName, "grand child page", childId);

        // View Page Test
        viewPage(TEST_SPACE_KEY, parentPageName);

        // Now check that child page appears in the parent page from use of children macro, and that the grandchild page doesn't appear
        assertTextPresent(childPageName);
        assertTextNotPresent(grandChildPageName);
    }

    public void testChildrenMacroWithInvalidDepth() throws Exception
    {
        createPage(TEST_SPACE_KEY, "newPage", "{children:depth=d}");
        viewPage(TEST_SPACE_KEY, "newPage");
        assertTextPresent("Unable to render {children}");
    }

    // TODOXHTML CONFDEV-1224
    public void TODOXHTML_testChildrenMacroForExternalSpace() throws IOException, XmlRpcException
    {
        createSpace(TEST_SPACE2_KEY, TEST_SPACE2_TITLE, TEST_SPACE2_DESCRIPTION);

        String parentPageName = "parentPage";
        String childPageName = "childPage";
        String grandChildPageName = "grandPage";

        setupPagesForChildMacro(TEST_SPACE2_KEY, parentPageName, childPageName, grandChildPageName);

        createPage(TEST_SPACE_KEY, "testChildrenExternal", "{children:all=true|page=" + TEST_SPACE2_KEY + ":}");
        viewPage(TEST_SPACE_KEY, "testChildrenExternal");

        assertLinkPresentWithText(parentPageName);
        assertLinkPresentWithText(childPageName);
        assertLinkPresentWithText(grandChildPageName);
        
        deleteSpace(TEST_SPACE2_KEY);
    }

    private void setupPagesForChildMacro(String spaceKey, String parentPageName, String childPageName, String grandChildPageName)
            throws IOException, XmlRpcException
    {
        long parentId = createPage(spaceKey, parentPageName, "{children:all=true}");
        long childId = createPage(spaceKey, childPageName, "child page", parentId);
        createPage(spaceKey, grandChildPageName, "grand child page", childId);
    }

    // TODOXHTML CONFDEV-1224
    public void TODOXHTML_testChildrenMacroForPageInOtherSpace() throws Exception
    {
        createUser(TEST_GEN_USERNAME1, TEST_GEN_PASSWORD1, TEST_GEN_USERNAME1);

        String parentPageName = "parentPage";
        String childPageName = "childPage";
        String grandChildPageName = "grandPage";
        setupPagesForChildMacro(TEST_SPACE_KEY, parentPageName, childPageName, grandChildPageName);

        createSpace(TEST_SPACE2_KEY, TEST_SPACE2_TITLE, TEST_SPACE2_DESCRIPTION);
        createPage(TEST_SPACE2_KEY, "testChildrenOtherSpace", "{children:all=true|page=" + TEST_SPACE_KEY + ":" + parentPageName + "}");

        gotoPage("/");
        // This logs us out of confluence
        grantGlobalBrowsePermissionToGroup("confluence-users");
        grantViewSpacePermissionToUser(TEST_SPACE2_KEY, TEST_GEN_USERNAME1);

        viewPage(TEST_SPACE2_KEY, "testChildrenOtherSpace");
        assertLinkNotPresentWithText(parentPageName);
        assertLinkPresentWithText(childPageName);
        assertLinkPresentWithText(grandChildPageName);
        
        logout();
        login(TEST_GEN_USERNAME1, TEST_GEN_PASSWORD1);
        viewPage(TEST_SPACE2_KEY, "testChildrenOtherSpace");
        assertLinkNotPresentWithText(childPageName);
        assertLinkNotPresentWithText(grandChildPageName);
        assertTextPresent("Page not found: " + TEST_SPACE_KEY + ":" + parentPageName);
        logout();
        loginAsAdmin();

        deleteSpace(TEST_SPACE2_KEY);
        deleteUser(TEST_GEN_USERNAME1);
    }

    // TODOXHTML CONFDEV-1224
	public void TODOXHTML_testChildrenMacroWithExcerpt() throws Exception
    {
        String excerptText = "This is an excerpt";

        // Setup test variables
        String parentPageName = "parentPage";
        String childPageName = "childPage";
        String grandChildPageName = "grandPage";

        long parentId = createPage(TEST_SPACE_KEY, parentPageName, "{children:all=true|excerpt=true}");
        long childId = createPage(TEST_SPACE_KEY, childPageName, "{excerpt}" + excerptText + "{excerpt}", parentId);
        createPage(TEST_SPACE_KEY, grandChildPageName, "grand child page", childId);

        viewPage(TEST_SPACE_KEY, childPageName); // view child page so excerpt is stored
        viewPage(TEST_SPACE_KEY, parentPageName);

        // Now check that child (and its excerpt) and grand child page appears in the parent page from use of children macro
        assertTextPresent(childPageName);
        assertTextPresent(excerptText);
        assertTextPresent(grandChildPageName);
    }

    // ADVMACROS-269
    public void testChildrenMacroWithShowDescendantsAndDepthOfDescendantsSetToAll()
    {
        String parentPageName = "parentPage";
        String childPageName = "childPage";
        String grandChildPageName = "grandPage";

        long parentId = createPage(TEST_SPACE_KEY, parentPageName, "{children:all=true|depth=all}");
        long childId = createPage(TEST_SPACE_KEY, childPageName, "Child page content", parentId);
        createPage(TEST_SPACE_KEY, grandChildPageName, "Grand Child page content", childId);

        viewPageById(parentId);

        assertLinkPresentWithExactText(childPageName);
        assertLinkPresentWithExactText(grandChildPageName);
    }
 }
