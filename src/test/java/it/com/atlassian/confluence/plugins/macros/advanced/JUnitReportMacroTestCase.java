package it.com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.plugin.functest.helper.AttachmentHelper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;


public class JUnitReportMacroTestCase extends AbstractConfluencePluginWebTestCaseBase
{

    private File jUnitReportXml;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        jUnitReportXml = copyTestJunitReportXmlToTempIoDirectory();
    }

    @Override
    protected void tearDown() throws Exception
    {
        try
        {
            jUnitReportXml.delete();
        }
        finally
        {
            super.tearDown();
        }
    }

    private File copyTestJunitReportXmlToTempIoDirectory() throws IOException
    {
        File tempFile = File.createTempFile(getClass().getName(), ".xml");

        FileUtils.copyURLToFile(
                getClass().getClassLoader().getResource("junitreport.xml"),
                tempFile
        );

        return tempFile;
    }

    private long createJUnitReportAsAttachmentToPage(long pageId)
            throws IOException
    {
        AttachmentHelper attachmentHelper = getAttachmentHelper();
        attachmentHelper.setParentId(pageId);
        attachmentHelper.setFileName(jUnitReportXml.getName());
        attachmentHelper.setContentType("text/xml");

        byte[] jUnitData = FileUtils.readFileToByteArray(jUnitReportXml);

        attachmentHelper.setContent(jUnitData);
        attachmentHelper.setContentLength(jUnitData.length);

        assertTrue(attachmentHelper.create());

        return attachmentHelper.getId();
    }


    private void assertReportTableHeading()
    {
        assertEquals(
                "Test",
                getElementTextByXPath("//div[@class='wiki-content']//table//tr/th")
        );
        assertEquals(
                "Time",
                getElementTextByXPath("//div[@class='wiki-content']//table//tr/th[2]")
        );
        assertEquals(
                "Messages",
                getElementTextByXPath("//div[@class='wiki-content']//table//tr/th[3]")
        );
    }


    public void testRenderJunitReport() throws IOException
    {
        // long testPageId = createPage(
                // TEST_SPACE_KEY, "testRenderJunitReport",
                // StringUtils.EMPTY
        // );

        // createJUnitReportAsAttachmentToPage(testPageId);

        // PageHelper pageHelper = getPageHelper(testPageId);
        // assertTrue(pageHelper.read());


        // ConfluenceWebTester confluenceWebTester = getConfluenceWebTester();
        // pageHelper.setContent("{junitreport:url=" + confluenceWebTester.getBaseUrl() + "/download/attachments/" + testPageId + "/" + jUnitReportXml.getName() + "?os_username=" + confluenceWebTester.getCurrentUserName() + "&os_password=" + confluenceWebTester.getCurrentPassword() + "}");

        // assertTrue(pageHelper.update());

        // viewPageById(testPageId);


        // assertReportTableHeading();

        // assertEquals(
                // "TestExampleMacro",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td")
        // );
        // assertEquals(
                // "50%",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td[2]")
        // );
        // assertEquals(
                // "00:00.26",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td[4]")
        // );
        // assertEquals(
                // "Tests: 2 Failures: 1 Exceptions: 0",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td[5]")
        // );

        // assertEquals(
                // "testBasic",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[3][@class='testpassed']/td")
        // );
        // assertEquals(
                // "00:00.2",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[3]/td[2]")
        // );

        // assertEquals(
                // "testBasicFail",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[4][@class='testfailed']/td")
        // );
        // assertEquals(
                // "00:00.2",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[4]/td[2]")
        // );

        // assertEquals(
                // "TestExampleMacro",
                // getElementTextByXPath("//div[@class='wiki-content']/p/table//tr/td")
        // );
        // assertEquals(
                // "50%",
                // getElementTextByXPath("//div[@class='wiki-content']/p/table//tr/td[2]")
        // );
        // assertEquals(
                // "00:00.26",
                // getElementTextByXPath("//div[@class='wiki-content']/p/table//tr/td[4]")
        // );
        // assertEquals(
                // "Tests: 2 Failures: 1 Exceptions: 0",
                // getElementTextByXPath("//div[@class='wiki-content']/p/table//tr/td[5]")
        // );
    }

    // public void testRenderJunitReportWithFailuresOnly() throws IOException
    // {
        // long testPageId = createPage(
                // TEST_SPACE_KEY, "testRenderJunitReportWithFailuresOnly",
                // StringUtils.EMPTY
        // );

        // createJUnitReportAsAttachmentToPage(testPageId);

        // PageHelper pageHelper = getPageHelper(testPageId);
        // assertTrue(pageHelper.read());


        // ConfluenceWebTester confluenceWebTester = getConfluenceWebTester();
        // pageHelper.setContent("{junitreport:reportdetail=failuresonly|url=" + confluenceWebTester.getBaseUrl() + "/download/attachments/" + testPageId + "/" + jUnitReportXml.getName() + "?os_username=" + confluenceWebTester.getCurrentUserName() + "&os_password=" + confluenceWebTester.getCurrentPassword() + "}");

        // assertTrue(pageHelper.update());

        // viewPageById(testPageId);


        // assertReportTableHeading();

        // assertEquals(
                // "TestExampleMacro",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td")
        // );
        // assertEquals(
                // "50%",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td[2]")
        // );
        // assertEquals(
                // "00:00.26",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td[4]")
        // );
        // assertEquals(
                // "Tests: 2 Failures: 1 Exceptions: 0",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td[5]")
        // );

        // assertEquals(
                // "testBasicFail",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[3][@class='testfailed']/td")
        // );
        // assertEquals(
                // "00:00.2",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[3]/td[2]")
        // );

        // assertEquals(
                // "TestExampleMacro",
                // getElementTextByXPath("//div[@class='wiki-content']/p/table//tr/td")
        // );
        // assertEquals(
                // "50%",
                // getElementTextByXPath("//div[@class='wiki-content']/p/table//tr/td[2]")
        // );
        // assertEquals(
                // "00:00.26",
                // getElementTextByXPath("//div[@class='wiki-content']/p/table//tr/td[4]")
        // );
        // assertEquals(
                // "Tests: 2 Failures: 1 Exceptions: 0",
                // getElementTextByXPath("//div[@class='wiki-content']/p/table//tr/td[5]")
        // );

        // assertElementNotPresentByXPath("//tr[@class='testpassed']");
    // }


    // public void testRenderJunitReportWithSummaryOnly() throws IOException
    // {
        // long testPageId = createPage(
                // TEST_SPACE_KEY, "testRenderJunitReportWithSummaryOnly",
                // StringUtils.EMPTY
        // );

        // createJUnitReportAsAttachmentToPage(testPageId);

        // PageHelper pageHelper = getPageHelper(testPageId);
        // assertTrue(pageHelper.read());


        // ConfluenceWebTester confluenceWebTester = getConfluenceWebTester();
        // pageHelper.setContent("{junitreport:reportdetail=summary|url=" + confluenceWebTester.getBaseUrl() + "/download/attachments/" + testPageId + "/" + jUnitReportXml.getName() + "?os_username=" + confluenceWebTester.getCurrentUserName() + "&os_password=" + confluenceWebTester.getCurrentPassword() + "}");

        // assertTrue(pageHelper.update());

        // viewPageById(testPageId);


        // assertReportTableHeading();

        // assertEquals(
                // "TestExampleMacro",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td")
        // );
        // assertEquals(
                // "50%",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td[2]")
        // );
        // assertEquals(
                // "00:00.26",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td[4]")
        // );
        // assertEquals(
                // "Tests: 2 Failures: 1 Exceptions: 0",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td[5]")
        // );

        // assertElementNotPresentByXPath("//div[@class='wiki-content']//table//tr[3]");
    // }


    // public void testRenderJunitReportWithAllDetails() throws IOException
    // {
        // long testPageId = createPage(
                // TEST_SPACE_KEY, "testRenderJunitReportWithAllDetails",
                // StringUtils.EMPTY
        // );

        // createJUnitReportAsAttachmentToPage(testPageId);

        // PageHelper pageHelper = getPageHelper(testPageId);
        // assertTrue(pageHelper.read());


        // ConfluenceWebTester confluenceWebTester = getConfluenceWebTester();
        // pageHelper.setContent("{junitreport:reportdetail=all|url=" + confluenceWebTester.getBaseUrl() + "/download/attachments/" + testPageId + "/" + jUnitReportXml.getName() + "?os_username=" + confluenceWebTester.getCurrentUserName() + "&os_password=" + confluenceWebTester.getCurrentPassword() + "}");

        // assertTrue(pageHelper.update());

        // viewPageById(testPageId);


        // assertReportTableHeading();

        // assertEquals(
                // "TestExampleMacro",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td")
        // );
        // assertEquals(
                // "50%",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td[2]")
        // );
        // assertEquals(
                // "00:00.26",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td[4]")
        // );
        // assertEquals(
                // "Tests: 2 Failures: 1 Exceptions: 0",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[2]/td[5]")
        // );

        // assertEquals(
                // "testBasic",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[3]/td[1]")
        // );
        // assertEquals(
                // "0%",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[3]/td[2]")
        // );
        // assertEquals(
                // "00:00.2",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[3]/td[4]")
        // );
        // assertEquals(
                // "Commands: 0 Failures: 0 Exceptions: 0",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[3]/td[5]")
        // );

        // assertEquals(
                // "testBasicFail",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[4]/td[1]")
        // );
        // /* Wtf? 2147483648% (div by zero)? */
       // assertEquals(
               // "0%",
               // getElementTextByXPath("//div[@class='wiki-content']//table//tr[3]/td[2]")
       // );
        // assertEquals(
                // "00:00.2",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[4]/td[4]")
        // );
        // assertEquals(
                // "Commands: 0 Failures: 1 Exceptions: 0",
                // getElementTextByXPath("//div[@class='wiki-content']//table//tr[4]/td[5]")
        // );

        // assertEquals(
                // "TestExampleMacro",
                // getElementTextByXPath("//div[@class='wiki-content']/p/table//tr/td")
        // );
        // assertEquals(
                // "50%",
                // getElementTextByXPath("//div[@class='wiki-content']/p/table//tr/td[2]")
        // );
        // assertEquals(
                // "00:00.26",
                // getElementTextByXPath("//div[@class='wiki-content']/p/table//tr/td[4]")
        // );
        // assertEquals(
                // "Tests: 2 Failures: 1 Exceptions: 0",
                // getElementTextByXPath("//div[@class='wiki-content']/p/table//tr/td[5]")
        // );
    // }
}
